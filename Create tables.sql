begin transaction

IF OBJECT_ID('model.reply', 'U') IS NOT NULL 
	DROP TABLE model.reply; 

IF OBJECT_ID('model.tag_topic', 'U') IS NOT NULL 
	DROP TABLE model.tag_topic; 

IF OBJECT_ID('model.topic', 'U') IS NOT NULL 
	DROP TABLE model.topic; 

IF OBJECT_ID('model.tag', 'U') IS NOT NULL 
	DROP TABLE model.tag; 

IF OBJECT_ID('model.user_forum', 'U') IS NOT NULL 
	DROP TABLE model.user_forum; 

CREATE TABLE model.user_forum (
    id INT PRIMARY KEY IDENTITY (1, 1),
    name NVARCHAR (25) NOT NULL,
    password VARCHAR (255) NOT NULL,
    surname NVARCHAR (50) NOT NULL,
	creation_date DATETIME NOT NULL,
    max_topic int,
    max_reply int
);

CREATE TABLE model.tag (
    id INT PRIMARY KEY IDENTITY (1, 1),
    name NVARCHAR (20) NOT NULL
);

CREATE TABLE model.topic (
    id INT PRIMARY KEY IDENTITY (1, 1),
    title NVARCHAR (50) NOT NULL,
    description TEXT NOT NULL,
    creation_date DATETIME NOT NULL,
	url_file VARCHAR(200),
	id_user INT NOT NULL,

    FOREIGN KEY (id_user) REFERENCES model.user_forum (id)
);

CREATE TABLE model.tag_topic (
    id INT PRIMARY KEY IDENTITY (1, 1),
    id_topic INT NOT NULL,
	id_tag INT NOT NULL,

	FOREIGN KEY (id_topic) REFERENCES model.topic (id),
	FOREIGN KEY (id_tag) REFERENCES model.tag (id)
);

CREATE TABLE model.reply (
    id INT PRIMARY KEY IDENTITY (1, 1),
	description varchar (2024) NOT NULL,
	creation_date DATETIME NOT NULL,
	url_file VARCHAR(200),
    id_topic INT NOT NULL,
	id_user INT NOT NULL,
	
	FOREIGN KEY (id_topic) REFERENCES model.topic (id),
	FOREIGN KEY (id_user) REFERENCES model.user_forum (id)
);

commit transaction
rollback transaction 
