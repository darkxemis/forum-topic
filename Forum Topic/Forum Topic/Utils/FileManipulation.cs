﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Forum_Topic.Utils
{
    public static class FileManipulation
    {
        /// <summary>
        /// Method that save the file into project/gallery folder
        /// </summary>
        /// <param name="file"></param>
        public static void saveFile(HttpPostedFileBase file, string path_to_save_file) {
            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    string path_file = System.IO.Path.Combine(path_to_save_file,
                                               Path.GetFileName(file.FileName));
                    file.SaveAs(path_file);
                }
                catch (Exception ex)
                {
                }
            }   
        }
    }
}