﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Forum_Topic.Utils
{
    /// <summary>
    /// Class Used to util common shared with diferent classes
    /// </summary>
    public static class LoginUtil
    {
        /// <summary>
        /// Method to encrypt a string
        /// </summary>
        /// <param name="password"></param>
        /// <returns> encrypted string</returns>
        public static string encryption(String password)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] encryptPass;
            UTF8Encoding encode = new UTF8Encoding();
            //Encrypt password string into Encrypted data  
            encryptPass = md5.ComputeHash(encode.GetBytes(password));
            StringBuilder encryptdata = new StringBuilder();

            //Create the new string encripted
            for (int i = 0; i < encryptPass.Length; i++)
            {
                encryptdata.Append(encryptPass[i].ToString());
            }
            return encryptdata.ToString();
        }
    }
}