﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Forum_Topic;
using Forum_Topic.Models;
using Forum_Topic.Utils;

namespace Forum_Topic.Controllers
{
    public class UserController : Controller
    {
        private Forum_TopicEntities db = new Forum_TopicEntities();

        /// <summary>
        /// Method used for call create view user
        /// </summary>
        /// <returns>view create user</returns>
        public ActionResult CreateUser()
        {
            return View("Create");
        }
        
        // POST: User/Create
        /// <summary>
        /// Method used for create an user
        /// </summary>
        /// <param name="user">user to save</param>
        /// <returns>view index login</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,password,surname")] User user)
        {
            user.setCreateDate(user);
            user.setPassword(user);
            user.setDefaultValueMaxTopicAndReply(user);

            if (ModelState.IsValid)
            {
                db.user_forum.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index", "Login");
            }

            return View(user);
        }

        // GET: User/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.user_forum.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.user_forum.Find(id);
            db.user_forum.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
