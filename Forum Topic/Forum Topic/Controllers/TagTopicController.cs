﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Forum_Topic;
using Forum_Topic.Models;

namespace Forum_Topic.Controllers
{
    public class TagTopicController : Controller
    {
        private Forum_TopicEntities db = new Forum_TopicEntities();

        // GET: TagTopic
        public ActionResult Index()
        {
            var tag_topic = db.tag_topic.Include(t => t.tag).Include(t => t.topic);
            return View(tag_topic.ToList());
        }

        // GET: TagTopic/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TagTopic tagTopic = db.tag_topic.Find(id);
            if (tagTopic == null)
            {
                return HttpNotFound();
            }
            return View(tagTopic);
        }

        // GET: TagTopic/Create
        public ActionResult Create()
        {
            ViewBag.id_tag = new SelectList(db.tag, "id", "name");
            ViewBag.id_topic = new SelectList(db.topic, "id", "title");
            return View();
        }

        // POST: TagTopic/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,id_topic,id_tag")] TagTopic tagTopic)
        {
            if (ModelState.IsValid)
            {
                db.tag_topic.Add(tagTopic);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_tag = new SelectList(db.tag, "id", "name", tagTopic.id_tag);
            ViewBag.id_topic = new SelectList(db.topic, "id", "title", tagTopic.id_topic);
            return View(tagTopic);
        }

        // GET: TagTopic/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TagTopic tagTopic = db.tag_topic.Find(id);
            if (tagTopic == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_tag = new SelectList(db.tag, "id", "name", tagTopic.id_tag);
            ViewBag.id_topic = new SelectList(db.topic, "id", "title", tagTopic.id_topic);
            return View(tagTopic);
        }

        // POST: TagTopic/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,id_topic,id_tag")] TagTopic tagTopic)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tagTopic).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_tag = new SelectList(db.tag, "id", "name", tagTopic.id_tag);
            ViewBag.id_topic = new SelectList(db.topic, "id", "title", tagTopic.id_topic);
            return View(tagTopic);
        }

        // GET: TagTopic/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TagTopic tagTopic = db.tag_topic.Find(id);
            if (tagTopic == null)
            {
                return HttpNotFound();
            }
            return View(tagTopic);
        }

        // POST: TagTopic/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TagTopic tagTopic = db.tag_topic.Find(id);
            db.tag_topic.Remove(tagTopic);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
