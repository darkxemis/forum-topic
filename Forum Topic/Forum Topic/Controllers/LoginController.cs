﻿using Forum_Topic.Models;
using Forum_Topic.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Forum_Topic.Controllers
{
    public class LoginController : Controller
    {
        private Forum_TopicEntities db = new Forum_TopicEntities();

        /// <summary>
        /// Method Login index, is used for display the index.cshtml of Login.
        /// </summary>
        /// <returns>index.cshtml template</returns>
        public ActionResult Index()
        {
            return View("Index");
        }

        /// <summary>
        /// Method used for validate the login user/password, password is encrypt with MD5 as you can see in Utils.LoginUtils.
        /// User and Id is saved as session for used when we create topics/replies.
        /// </summary>
        /// <param name="user"> The current user we are trying to log in</param>
        /// <returns>If login is failed return error otherwhise the view for the topics</returns>
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ValidateLogin(User user)
        {
            try
            {
                string passwordMD5 = LoginUtil.encryption(user.password);
                
                User user_found = db.user_forum
                                .Where(s => s.name == user.name && s.password == passwordMD5)
                                .FirstOrDefault<User>();

                if (user_found != null) 
                {
                    Session["User"] = user_found.name;
                    Session["User_Id"] = user_found.id;
                    return RedirectToAction("Index", "Topic");
                } else
                {
                    ModelState.AddModelError(string.Empty, "Incorrect user name or password.");
                    return View("Index");
                }

            }
            catch
            {
                return View("Index");
            }
        }
    }
}
