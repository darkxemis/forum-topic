﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Forum_Topic;
using Forum_Topic.Models;
using Forum_Topic.Utils;

namespace Forum_Topic.Controllers
{
    public class ReplyController : Controller
    {
        private Forum_TopicEntities db = new Forum_TopicEntities();

        /// <summary>
        /// Method create a new replay
        /// </summary>
        /// <param name="reply">reply to save</param>
        /// <param name="id_topic">idtopic assigned to that reply</param>
        /// <returns>Redirect to details of topic</returns>
        public ActionResult CreateReply(Reply reply, int id_topic, HttpPostedFileBase url_file)
        {
            /*Asing Topic to reply*/
            reply.addUserToReply(reply, id_topic);

            reply.setCreateDate(reply);

            if (url_file != null)
            {
                FileManipulation.saveFile(url_file, Server.MapPath("~/gallery"));
                reply.setUrlFile(reply, url_file.FileName);
            }

            /*Add user to reply*/
            if (Session["User_Id"] != null)
                reply.id_user = Int32.Parse(Session["User_Id"].ToString());
            else
                reply.id_user = 1; // If we forget to login all topic is created for administrator

            /*Increments max_reply*/
            User user = db.Users.Find(reply.id_user);
            user.incrementMaxReply(user);

            if (ModelState.IsValid)
            {
                db.Replies.Add(reply);
                db.SaveChanges();
                //return RedirectToAction("Details", "Topic", id_topic);
                return RedirectToAction("Details", new RouteValueDictionary(
                    new { controller = "Topic", action = "Details", Id = id_topic }));
            }

            return View();
        }

        // GET: Reply/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reply reply = db.reply.Find(id);
            if (reply == null)
            {
                return HttpNotFound();
            }
            return View(reply);
        }

        // POST: Reply/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reply reply = db.reply.Find(id);
            reply.user_forum.decrementMaxReply(reply.user_forum);
            db.reply.Remove(reply);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
