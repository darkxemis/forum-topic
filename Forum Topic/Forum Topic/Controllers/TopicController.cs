﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Forum_Topic;
using Forum_Topic.Models;
using Forum_Topic.Utils;

namespace Forum_Topic.Controllers
{
    public class TopicController : Controller
    {
        private Forum_TopicEntities db = new Forum_TopicEntities();

        /// <summary>
        /// Index view after login, we show the most recently Topics created in the system
        /// </summary>
        /// <returns>Index view with all recently topics</returns>
        public ActionResult Index()
        {
            List<Topic> AllTopicsListOrdered = new List<Topic>();

            using (var context = db)
            {
                AllTopicsListOrdered = context.Topics
                            .OrderByDescending(x => x.creation_date)
                            .Include(y => y.user_forum)
                            .ToList<Topic>();
            }

            return View(AllTopicsListOrdered);
        }

        /// <summary>
        /// Show top 10 topic by user in the system
        /// </summary>
        /// <returns></returns>
        public ActionResult TopTopicUser()
        {
            List<User> AllTopicsListOrdered = new List<User>();

            try
            {
                using (var context = db)
                {
                    AllTopicsListOrdered = context.Users
                               .OrderByDescending(x => x.max_topic)
                               .Take(10)
                               .ToList<User>();
                }
            } catch (Exception ex)
            {

            }


            return View("MostTopicUser", AllTopicsListOrdered);
        }

        /// <summary>
        /// Search method to find the title of whatever topic.
        /// </summary>
        /// <param name="search_name"></param>
        /// <returns>Index with the topic filtered</returns>
        public ActionResult Search(string search_name)
        {
            List<Topic> AllTopicsListOrdered = new List<Topic>();

            try
            {
                using (var context = db)
                {
                    AllTopicsListOrdered = context.Topics
                              .OrderByDescending(x => x.title)
                              .Where(b => b.title.Trim().ToLower().Contains(search_name.Trim().ToLower()))
                              .Include(y => y.user_forum)
                              .ToList<Topic>();
                }
            } catch (Exception ex)
            {

            }

            return View("Index", AllTopicsListOrdered);
        }


        /// <summary>
        /// Detail view for topics also you can reply in this view for a specific topic
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Topic topic = db.Topics.Find(id);
            BigModel model = new BigModel();
            model.topic = topic;

            if (topic == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        /// <summary>
        /// Method who call the view for create a new topic
        /// </summary>
        /// <returns>create view</returns>
        public ActionResult CreateForm()
        {
            return View("Create");
        }

        // POST: Topic/Create
        /// <summary>
        /// Method that create a new topic, it is assssigned to a user logged but if the user is not, then by default is the user 1
        /// </summary>
        /// <param name="topic">The topic we want to save</param>
        /// <returns>view index topic</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,title,description")] Topic topic, HttpPostedFileBase url_file)
        {
            topic.setCreateDate(topic);

            if (url_file != null)
            {
                FileManipulation.saveFile(url_file, Server.MapPath("~/gallery"));
                topic.setUrlFile(topic, url_file.FileName);
            }
            
            if (Session["User_Id"] != null)
                topic.id_user = Int32.Parse(Session["User_Id"].ToString());
            else
                topic.id_user = Int32.Parse(Session["User_Id"].ToString()); // If we forget to login all topic is created for administrator

            User user = new User();

            try
            {
                user = db.Users.Find(topic.id_user);
                user.incrementMaxTopic(user); //Increments max topic of user, reference objects is passed so it's going to be modify
            } catch (Exception ex)
            {

            }

            if (ModelState.IsValid)
            {
                db.Topics.Add(topic);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(topic);
        }

        /// <summary>
        /// Method that dowload the file linked to a topic
        /// </summary>
        /// <param name="file_name"></param>
        /// <returns> File to dowload</returns>
        public FileResult DowloadFile(string file_name) {
            string path_file = Path.Combine(Server.MapPath("~/gallery"),
                                               Path.GetFileName(file_name));

            byte[] fileBytes = System.IO.File.ReadAllBytes(@path_file);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, file_name);
        }

        // GET: Topic/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Topic topic = db.Topics.Find(id);
            if (topic == null)
            {
                return HttpNotFound();
            }
            return View(topic);
        }

        // POST: Topic/Edit/5
        /// <summary>
        /// Method used to edit a topic
        /// </summary>
        /// <param name="topic">Topic we want to edit</param>
        /// <returns>view index topic</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,title,description")] Topic topic)
        {
            topic.creation_date = DateTime.Now;

            if (Session["User_Id"] != null)
                topic.id_user = Int32.Parse(Session["User_Id"].ToString());
            else
                topic.id_user = 1; // If we forget to login all topic is created for administrator

            if (ModelState.IsValid)
            {
                db.Entry(topic).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(topic);
        }

        // GET: Topic/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Topic topic = db.Topics.Find(id);
            if (topic == null)
            {
                return HttpNotFound();
            }
            return View(topic);
        }

        /// <summary>
        /// Delete the topic with all reply assigned to it
        /// </summary>
        /// <param name="id"></param>
        /// <returns>view index topic</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Topic topic = db.Topics.Find(id);
            topic.user_forum.decrementMaxTopic(topic.user_forum); //Derenent max topic when is deleted

            //Delete all foreing key from reply
            foreach (Reply reply in topic.reply.ToList())
            {
                db.Replies.Remove(reply);
            }

            db.Topics.Remove(topic);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
