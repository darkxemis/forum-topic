﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forum_Topic.Models
{
    public class BigModel
    {
        public Topic topic { get; set; }
        public Reply reply { get; set; }
    }
}