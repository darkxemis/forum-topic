﻿namespace Forum_Topic.Models
{
    using Forum_Topic.Models;
    using Forum_Topic.Utils;
    using System;
    using System.Collections.Generic;

    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            this.reply = new HashSet<Reply>();
            this.topic = new HashSet<Topic>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public string password { get; set; }
        public string surname { get; set; }
        public System.DateTime creation_date { get; set; }
        public Nullable<int> max_topic { get; set; }
        public Nullable<int> max_reply { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Reply> reply { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Topic> topic { get; set; }

        /// <summary>
        /// Method that increment max topic
        /// </summary>
        /// <param name="user"></param>
        public void incrementMaxTopic(User user)
        {
            if (user.max_topic != null)
                user.max_topic += 1;
            else
                user.max_topic = 1;
        }

        /// <summary>
        /// Method that increment max reply
        /// </summary>
        /// <param name="user"></param>
        public void incrementMaxReply(User user)
        {
            user.max_reply += 1;
        }

        /// <summary>
        /// Method that decrement max reply
        /// </summary>
        /// <param name="user"></param>
        public void decrementMaxReply(User user)
        {
            user.max_reply -= 1;
        }

        /// <summary>
        /// Method that decrement max topic
        /// </summary>
        /// <param name="user"></param>
        public void decrementMaxTopic(User user)
        {
            user.max_topic -= 1;
        }

        /// <summary>
        /// Method that create the current date time
        /// </summary>
        /// <param name="User"></param>
        public void setCreateDate(User user)
        {
            user.creation_date = DateTime.Now;
        }

        /// <summary>
        /// Method that create encrypt password
        /// </summary>
        /// <param name="User"></param>
        public void setPassword(User user)
        {
            user.password = LoginUtil.encryption(user.password);
        }

        /// <summary>
        /// Method that set value max_topic and max_replay to 0
        /// </summary>
        /// <param name="User"></param>
        public void setDefaultValueMaxTopicAndReply(User user)
        {
            user.max_topic = 0;
            user.max_reply = 0;
        }
    } 
}
