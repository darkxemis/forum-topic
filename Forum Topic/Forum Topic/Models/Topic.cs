﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forum_Topic.Models
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class Topic
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Topic()
        {
            this.reply = new HashSet<Reply>();
            this.tag_topic = new HashSet<TagTopic>();
        }

        public int id { get; set; }
        public string title { get; set; }
        [AllowHtml]
        public string description { get; set; }
        public System.DateTime creation_date { get; set; }

        public string url_file { get; set; }
        public int id_user { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Reply> reply { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TagTopic> tag_topic { get; set; }
        public virtual User user_forum { get; set; }

        /// <summary>
        /// Method that create the current date time
        /// </summary>
        /// <param name="topic"></param>
        public void setCreateDate (Topic topic)
        {
            topic.creation_date = DateTime.Now;
        }

        /// <summary>
        /// Method that create url
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="file_name"></param>
        public void setUrlFile(Topic topic, string file_name)
        {
            topic.url_file = file_name;
        }

    }
}