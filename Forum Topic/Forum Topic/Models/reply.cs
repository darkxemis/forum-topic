﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Forum_Topic.Models
{
    public class Reply
    {
        [Key]
        public int id { get; set; }
        public string description { get; set; }
        public System.DateTime creation_date { get; set; }

        public string url_file { get; set; }
        public int id_topic { get; set; }
        public int id_user { get; set; }

        public virtual Topic topic { get; set; }
        public virtual User user_forum { get; set; }

        /// <summary>
        /// Method that create the current date time
        /// </summary>
        /// <param name="reply"></param>
        public void setCreateDate(Reply reply)
        {
            reply.creation_date = DateTime.Now;
        }

        /// <summary>
        /// Method that add an reply to a topic
        /// </summary>
        /// <param name="reply"></param>
        /// <param name="id_topic"></param>
        public void addUserToReply(Reply reply, int id_topic)
        {
            reply.id_topic = id_topic;
        }

        /// <summary>
        /// Method that create url
        /// </summary>
        /// <param name="reply"></param>
        /// <param name="file_name"></param>
        public void setUrlFile(Reply reply, string file_name)
        {
            reply.url_file = file_name;
        }
    }
}