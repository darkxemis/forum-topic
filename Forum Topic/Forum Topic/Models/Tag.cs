﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forum_Topic.Models
{
    using System;
    using System.Collections.Generic;
    public class Tag
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tag()
        {
            this.tag_topic = new HashSet<TagTopic>();
        }

        public int id { get; set; }
        public string name { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TagTopic> tag_topic { get; set; }
    }
}