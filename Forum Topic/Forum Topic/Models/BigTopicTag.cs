﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forum_Topic.Models
{
    public class BigTopicTag
    {
        public Topic topic { get; set; }
        public ICollection<Tag> tags { get; set; }
    }
}