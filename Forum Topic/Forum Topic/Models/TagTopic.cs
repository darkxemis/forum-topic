﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forum_Topic.Models
{
    public class TagTopic
    {
        public int id { get; set; }
        public int id_topic { get; set; }
        public int id_tag { get; set; }

        public virtual Tag tag { get; set; }
        public virtual Topic topic { get; set; }
    }
}